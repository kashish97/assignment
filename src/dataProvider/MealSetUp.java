package dataProvider;

import java.util.ArrayList;
import java.util.List;

import model.Dish;
import model.Ingredient;
import model.Meal;

public class MealSetUp {
	static List<Meal>  mealList = new ArrayList<Meal>();
	
	
	public void setDishData() throws Exception {
		Dish butterChicken = new Dish("Butter Chicken");
		try {
	
	butterChicken.addIngredient(new Ingredient(IngredientName.BUTTER,1));
	butterChicken.addIngredient(new Ingredient(IngredientName.CHICKEN,1));
	butterChicken.addIngredient(new Ingredient(IngredientName.ONION,5));
	butterChicken.addIngredient(new Ingredient(IngredientName.GARLIC,5));
	butterChicken.addIngredient(new Ingredient(IngredientName.GINGER,2));
		}
		catch(Exception e) {
			throw new Exception(butterChicken.getdName()+" cannot be prepared because "+e.getMessage());
		}
		Dish paneerButter = new Dish("Paneer Butter Masala");
		
		try {
	
	paneerButter.addIngredient(new Ingredient(IngredientName.BUTTER,1));
	paneerButter.addIngredient(new Ingredient(IngredientName.PANEER,1));
	paneerButter.addIngredient(new Ingredient(IngredientName.ONION,5));
	paneerButter.addIngredient(new Ingredient(IngredientName.GARLIC,5));
	paneerButter.addIngredient(new Ingredient(IngredientName.GINGER,2));
		}
		catch(Exception e) {
			throw new Exception(paneerButter.getdName()+" cannot be prepared because "+e.getMessage());
		}
		Dish roti = new Dish("Roti");
		try {
	
	roti.addIngredient(new Ingredient(IngredientName.WHEAT,1));
		}
		catch(Exception e) {
			throw new Exception(roti.getdName()+" cannot be prepared because "+e.getMessage());
		}
		Dish rice = new Dish("Jeera Rice");
		try {
	
	rice.addIngredient(new Ingredient(IngredientName.RICE,1));
	
		}
		catch(Exception e) {
			throw new Exception(rice.getdName()+" cannot be prepared because "+e.getMessage());
		}

		Meal meal1 = new Meal(1);
		meal1.addDishToMeal(butterChicken);
		meal1.addDishToMeal(roti);
		
		
		Meal meal2 = new Meal(2);
		meal2.addDishToMeal(rice);
		meal2.addDishToMeal(butterChicken);
		
		
		Meal meal3 = new Meal(3); 
		meal3.addDishToMeal(paneerButter);
		meal3.addDishToMeal(rice);
		
		
		Meal meal4 = new Meal(4);
		meal4.addDishToMeal(paneerButter);
		meal4.addDishToMeal(roti);
		
		mealList.add(meal1);
		mealList.add(meal2);
		mealList.add(meal3);
		mealList.add(meal4);

		
		
			}
	
	public static List<Meal> getMealsAvailable(){
	
		return mealList;
	}
	}
	
