package userInterface;

import dataProvider.MealSetUp;
import dataProvider.IngredientStoreManagement;

public class Main {
	
	public static void main(String args[]) {
		try {
			
		IngredientStoreManagement.setIngredientData();
		MealSetUp dishData = new MealSetUp();
		dishData.setDishData();
		UserInterface ui = new UserInterface();
		int total=ui.cartCalculator();
		System.out.println(total);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
