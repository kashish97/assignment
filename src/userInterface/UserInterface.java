package userInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Scanner;
import java.util.Set;

import dataProvider.MealSetUp;
import model.Dish;
import model.Ingredient;
import model.Meal;


public class UserInterface {
	
	
	public int cartCalculator() throws Exception {
		
		
		 List<Meal> meals = MealSetUp.getMealsAvailable();
		 System.out.println("Choose a Meal");
		 Map<Ingredient,Integer> map1 = new HashMap<>();
	        for(int i=0; i<meals.size(); i++){
	            Meal meal = meals.get(i);
	            System.out.println("");
	            System.out.print((i+1)+"  ");
	            //System.out.println(meal.ge);
	            for(Dish dish : meal.getDishlist()) {
	            	if(dish==meal.getDishlist().get(meal.getDishlist().size()-1)) System.out.print(dish.getdName());
	            	else System.out.print(dish.getdName() + "+");
	            }
	        }

	        Scanner scanner = new Scanner(System.in);
	        String inp = scanner.next();

	        String[] inputStream = inp.split(",");

	        List<Ingredient> cart = new ArrayList<>();

	      
	        	
	            for(String a : inputStream){
	            	int count=0;
	            	 for(int k =0; k<meals.size(); k++){
	            	if(a.matches("[^\\d]")){
	            		throw new Exception("Wrong Input");
	            	}
	                if(meals.get(k).getMealId()==Integer.parseInt(a)){
	                    Meal m = meals.get(k);
	                    for(Dish d: m.getDishlist()){
	                        
	                    	for(Ingredient i : d.getIngredientsData()){
	                    	
	                    		if(map1.keySet().contains(i)) {
	                    			
	                    			map1.put(i, map1.get(i)+i.getQty());
	                    		}
	                    		
	                    		else {
	                    			map1.put(i, i.getQty());
	                    		}
	                            }
	                        }
	                    }
	                else
	                {count++;
	                }
	                	
	                }
	            if(count==meals.size()) throw new Exception("YOU CHOSED WRONG MEAL "+a+ "   is not available in our cart");

	                
	            }
	       
	            System.out.println("Ingredients    " +"Quantity     "+"Price");
	 	       
	            Set<Ingredient> set1= map1.keySet();
	            int total=0;
	            for(Ingredient i:set1) {
	            	 System.out.println(i.getiName()+"            " + map1.get(i)+"          "+(map1.get(i)* i.getPrice()));
	            	 total+=map1.get(i)* i.getPrice();
	     	        
	            }

	
	      
	          return total; 


	 

		
	}
public static Ingredient check(String name, List<Ingredient> cart){
    for(Ingredient i: cart){
        if(name.equals(i.getiName())){
            return i;
        }
    }
    return null;
}
	

}
