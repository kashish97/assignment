package model;
import java.util.ArrayList;
import java.util.List;




public class Dish {
    private String dName;
    private List<Ingredient> ingredientsData=new ArrayList<>();

    public Dish(String dName) {
        this.dName = dName;
    }


    public String getdName() {
        return dName;
    }

    public List<Ingredient> getIngredientsData() {
        return ingredientsData;
    }

    public void addIngredient(Ingredient ingredient) {    
        ingredientsData.add(ingredient);  
 }  
}
