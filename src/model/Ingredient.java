package model;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import dataProvider.IngredientName;


public class Ingredient {
	    private IngredientName iName;
	    private Integer price;
	    private Integer qty;
	    private static Map<IngredientName,Integer> ingredient_stats = new HashMap<>();

	    

		public Ingredient(IngredientName iName, int qty) throws Exception {
	       
	        this.iName = iName;
	        this.qty = qty;
	    
	        this.price=0;
	        	Set <IngredientName> ingset = ingredient_stats.keySet();
	        
	        	if(!ingset.contains(iName)) {
	        	
	        		throw new Exception(iName+ " is not Available.");
	        		
	        	}
	        	else {
	        		this.price = ingredient_stats.get(iName);
	        	}
	        
	    }

	  
	    @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((iName == null) ? 0 : iName.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Ingredient other = (Ingredient) obj;
			if (iName != other.iName)
				return false;
			return true;
		}


		public IngredientName getiName() {
	        return iName;
	    }


	    public int getPrice() {
	        return price;
	    }


	    public int getQty() {
	        return qty;
	    }

	    public void setQty(int qty) {
	        this.qty = qty;
	    }

		public static Map<IngredientName,Integer> getIngredient_stats() {
			return ingredient_stats;
		}
		
		public static void addIngredients(IngredientName ingname, Integer price) {
			Ingredient.ingredient_stats.put(ingname, price);
		}
}
