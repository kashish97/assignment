package model;

import java.util.ArrayList;
import java.util.List;

public class Meal {
	
	private Integer mealId;
	private List<Dish> dishlist = new ArrayList<>();

	public Meal(Integer mealId) {
		this.mealId = mealId;
	}

	public Integer getMealId() {
		return mealId;
	}
	
	public List<Dish> getDishlist() {
		
		return dishlist;
	}

	public void addDishToMeal(Dish dish) {
		
		dishlist.add(dish);
		
		
	}

}





