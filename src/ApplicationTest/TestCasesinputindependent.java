package ApplicationTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import dataProvider.IngredientName;
import dataProvider.IngredientStoreManagement;
import dataProvider.MealSetUp;
import model.Dish;
import model.Ingredient;
import userInterface.UserInterface;

public class TestCasesinputindependent {
@Rule 
public ExpectedException ee=ExpectedException.none();

@Test
public void testAddIngredient() throws Exception
{
IngredientStoreManagement.setIngredientData();
IngredientName iName=IngredientName.SALT;
ee.expect(Exception.class);
ee.expectMessage(iName+ " is not Available.");
Ingredient obj=new Ingredient(iName,3);
	
}
@Test
public void testAddDish() throws Exception
{
IngredientStoreManagement.setIngredientData();
IngredientName iName=IngredientName.SALT;
Dish butterChicken = new Dish("Butter Chicken");
ee.expect(Exception.class);
ee.expectMessage(butterChicken.getdName()+" cannot be prepared because "+iName+ " is not Available.");
try{
butterChicken.addIngredient(new Ingredient(IngredientName.BUTTER,1));
butterChicken.addIngredient(new Ingredient(IngredientName.CHICKEN,1));
butterChicken.addIngredient(new Ingredient(IngredientName.ONION,5));
butterChicken.addIngredient(new Ingredient(IngredientName.GARLIC,5));
butterChicken.addIngredient(new Ingredient(IngredientName.GINGER,2));
butterChicken.addIngredient(new Ingredient(iName,3));
	
}

catch(Exception e) {
	throw new Exception(butterChicken.getdName()+" cannot be prepared because "+e.getMessage());
}


}

}